package com.example.DuplicateCheckService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.example.duplicatecheck.DuplicateCheckServiceApplication;
import com.example.duplicatecheck.controller.DuplicateCheckController;

@SpringBootTest(classes = DuplicateCheckServiceApplication.class)
@RunWith(SpringRunner.class)
public class DuplicateCheckServiceApplicationTest {

	private MockMvc mockMvc;

	@Autowired
	protected WebApplicationContext wac;

	@Autowired
	private DuplicateCheckController duplicateCheckController;

	
	@Before
	public void setup() throws Exception {
		this.mockMvc = standaloneSetup(this.duplicateCheckController).build();
	}
	
	@Test
	public void testFindKeys() throws Exception {
		mockMvc.perform(get("/service/duplicateCheck/9999999999999916").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
}
