package com.example.duplicatecheck.repo;

import java.util.Map;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Vinay Kariyappa Created On 2019-11-25
 */
@Repository
public class KeysRepository {

	public static final String KEY = "ITEM";
	private RedisTemplate<String, String> redisTemplate;
	private HashOperations hashOperations;

	public KeysRepository(RedisTemplate<String, String> redisTemplate) {
		this.redisTemplate = redisTemplate;
		hashOperations = redisTemplate.opsForHash();
	}

	/* Getting a specific item by item id from table */
	public String getKey(String key) {
		return (String) hashOperations.get(KEY, key);
	}

	/* Adding an item into redis database */
	public void addKey(String key) {
		hashOperations.put(KEY, key, key);
	}
	
}
