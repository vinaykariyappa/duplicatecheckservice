package com.example.duplicatecheck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.example" })
public class DuplicateCheckServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DuplicateCheckServiceApplication.class, args);
	}

}
