package com.example.duplicatecheck.controller;

import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.duplicatecheck.model.ApiResponse;
import com.example.duplicatecheck.repo.KeysRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * @author Vinay Kariyappa Created On 2019-11-25
 */
@RestController
@RequestMapping(value = "service")
@Validated
@Api(value = "Duplicate Check API", description = "Duplicate Check API")
public class DuplicateCheckController {

	private static Logger LOGGER = LoggerFactory.getLogger(DuplicateCheckController.class);

	@Autowired
	private KeysRepository keysRepository;

	/**
	 * This Method for duplicate check
	 * 
	 * @param key
	 * @return
	 */
	@ApiOperation(value = "Check duplicate entry of Key")
	@RequestMapping(value = "/duplicateCheck/{key}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> duplicateCheck(
			@ApiParam(value = "requested Key to check", required = true) 
			@PathVariable(value = "key", required = true) @Size(min = 1, max = 20, message = "key length must be between 1 and 20") String key) {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("::::::::DuplicateCheckController:::::::::duplicateCheck::::START");
		}
		ApiResponse apiResponse = new ApiResponse();
		apiResponse.setKeys(key);

		/* Calling repository to get Key for check key is exist or not */
		String result = keysRepository.getKey(key);

		if (result != null) {
			apiResponse.setMessage("Key is already available!!!!!");
			apiResponse.setStatus(true);
		} else {
			keysRepository.addKey(key);
			apiResponse.setMessage("Key is not availabe inserted successfully!!!!!");
			apiResponse.setStatus(false);
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("::::::::DuplicateCheckController:::::::::duplicateCheck::::END");
		}
		return new ResponseEntity<Object>(apiResponse, HttpStatus.OK);
	}
}
