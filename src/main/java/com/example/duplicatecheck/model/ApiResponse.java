package com.example.duplicatecheck.model;

import java.util.Date;
import java.util.Set;

/**
 * 
 * @author Vinay Kariyappa Created On 2019-11-25
 */
public class ApiResponse {

	private Date timeStamp;
	private boolean status;
	private String message;
	private Object keys;

	public ApiResponse() {
		super();
		this.timeStamp = new Date();
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getKeys() {
		return keys;
	}

	public void setKeys(Object keys) {
		this.keys = keys;
	}

}
